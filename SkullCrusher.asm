; Lab3 Part 2 CS2290

INCLUDE Irvine32.inc
INCLUDE VirtualKeys.inc

	RIGHT 		equ 79
	LEFT		equ 1
	
.data
score				BYTE 0
bank				BYTE 50

plr					BYTE 01h

delta				BYTE 1
pos					BYTE 0

walls				WORD ?

speed				DWORD 100

gameOverS			BYTE "Game Over!", 0
scoreS				BYTE "Score: ", 0
bankS				BYTE "Bank: ", 0
deltaDebugS			BYTE "Delta: ", 0
posDebugS			BYTE "Pos: ", 0

gameOverReason		BYTE 0

emptyBankS			BYTE "Ran out of bank!", 0
hitWallS			BYTE "Hit walls!", 0
hitEscapeS			BYTE "Hit Escape", 0

statsHeaderS 		BYTE "Bounce by Bounce Analysis", 13, 10, 201, 8 dup(205), 203, 8 dup(205), 187, 13, 10,
							186, ' ', "Bounce", ' ', 186, ' ', ' ', "Score", ' ', 186, 13, 10,
							204, 8 dup(205), 206, 8 dup(205), 185, 13, 10, 0

stats				BYTE 20 dup(?)


.code
main PROC


	call	ClrScr
	
	mov		ax,	0
	mov 	ah, 	79
	mov		walls, 	ax
	
	anim:
		
		mov			eax, speed			; Delay by 100 milliseconds before drawing the next frame
		call		Delay
	
	
		call		DrawPlayer
		
		call		ReadKey
		cmp			dx, 'A'
		je			bounceA				; Bounce the player if user presses A key
		cmp			dx, 'L'
		je			bounceL				; Bounce the player if the user pressed L key
		cmp			dx, 01bh
		jne			notEscape
		call		PressEscape
		jmp 		gameOver
		
	notEscape:
		cmp			dx, ' '
		jne			notPaused
		call		PauseGame
	
	notPaused:
		call		DrawWalls
		
		call		DrawUI
		
		mov 		ax,	walls
	
		cmp			pos, al				; Check the Left Bounds
		jae 		notLeft
		mov			gameOverReason, 2
		jmp			gameOver
	notLeft:
		cmp			pos, ah				; Check the Right Bounds
		jbe			notRight
		mov			gameOverReason, 2
		jmp			gameOver
	notRight:
		cmp			bank, 0
		jnz 		anim

		mov			gameOverReason, 1
	gameOver:						; Game over, print Game Over Screen
		call		ClrScr
		mov			edx, 0
		mov			dl, 30
		mov			dh, 10
		call		Gotoxy
		mov			edx, offset gameOverS
		call		WriteString
		
		mov			edx, 0
		mov			dl, 30
		mov			dh, 11
		call		Gotoxy
		mov			edx, offset scoreS
		call		WriteString
		movzx		eax, score
		call		WriteDec
		call		PrintExitReason
		
		mov			ecx, 5
		entera:
			call		Crlf
		loop 		entera
		
		call		DisplayStats
		
		call		WaitMsg
		
		exit
	bounceA:
	;	Make the player bounce when close to the screen
		mov 		al, delta
		cmp			al, 0
		jg			anim
		
		mov			dl, pos
		mov			dh, 5
		call		Gotoxy
		sub			dl, delta
		mov			al, ' '
		call		WriteChar
		inc 		score
		neg			delta
		mov			ax,	walls
		mov			bl,	pos
		sub			bl, al
		mov			ecx, offset stats
		movzx		esi, score
		mov			BYTE ptr [ecx + esi], bl
		cmp			bl, 1
		jz			anim
		
		sub			bank, bl
		jnc			safea
		mov			gameOverReason, 1
		jmp			gameOver
		
	safea:
		
		inc			al
		mov 		al, pos
		mov			walls, 	ax
		
		call		CalcSpeed

		jmp			anim
	
	bounceL:
		mov 		al, delta
		cmp			al, 0
		jl			anim
		
		mov			dl, pos
		mov			dh, 5
		call		Gotoxy
		sub			dl, delta
		mov			al, ' '
		call		WriteChar
		
		
		neg			delta
		mov			ax, walls
		mov			bl, pos
		mov			bh, ah
		sub			bh, bl
		mov			ecx, offset stats
		movzx		esi, score
		mov			BYTE ptr [ecx + esi], bh

		cmp			bl, 1
		jz			anim
		sub			bl, ah
		neg			bl
		sub			bank, bl
		jnc			safe
		mov			gameOverReason, 1
		jmp			gameOver
		
	safe:
		dec			ah
		mov			ah, pos
		mov			walls, 	ax
		inc 		score
		
		call		CalcSpeed
		
		jmp			anim	

	exit

main ENDP

DisplayStats PROC
		pushad
		movzx		ecx, score
		mov			edx, 0
		call		Gotoxy
		mov			edx, offset statsHeaderS
		call		WriteString
		movzx		ecx, score
		mov			ebx, 0
	statsLoop:
		cmp			ecx, 0
		jz			outOfStats	
		mov			al, 186		
		call		WriteChar
		mov			eax, ebx					; Print the Bounce number
		inc			eax
		cmp			eax, 10
		jae			greaterTen
		push		eax
		mov			al, ' '
		call		WriteChar
		pop			eax
	greaterTen:
		push		eax
		mov			al, ' '
		call		WriteChar
		call		WriteChar
		call		WriteChar
		pop			eax		
	
		call		WriteDec

		mov			al, ' '
		call		WriteChar
		call		WriteChar
		call		WriteChar
		mov			al, 186
		call		WriteChar
		mov			al, ' '
		call		WriteChar
		call		WriteChar
		call		WriteChar
		mov			esi, offset stats
		movzx		eax, BYTE ptr [esi + ebx]
		call		WriteDec
		cmp			eax, 10
		jae			lessTen
		mov			al, ' '
		call		WriteChar
	lessTen:	
		mov			al, ' '
		call		WriteChar
		call		WriteChar
		call		WriteChar
		mov			al, 186
		call		WriteChar
		inc			ebx
		call		Crlf

		cmp		ecx, 0
		jl 		outOfStats
		dec		ecx
		jmp		statsLoop
	outOfStats:
		mov 	al, 200
		call	WriteChar
		mov		al, 205
		call	WriteChar
		call	WriteChar
		call	WriteChar
		call	WriteChar
		call	WriteChar
		call	WriteChar
		call	WriteChar
		call	WriteChar
		mov		al, 202
		call	WriteChar
		mov		al, 205
		call	WriteChar
		call	WriteChar
		call	WriteChar
		call	WriteChar
		call	WriteChar
		call	WriteChar
		call	WriteChar
		call	WriteChar		
		mov		al, 188
		call	WriteChar
		call	Crlf
		popad
		ret


DisplayStats ENDP

CalcSpeed PROC

		mov			eax, speed
		cmp			eax, 40
		jg			speed40
		cmp			eax, 20
		jg			speed20
		cmp			eax, 10
		jg			speed10
		cmp			eax, 5
		jg			speed5
		jmp			speed1
		
	speed40:
		sub			eax, 5
	speed20:
		sub			eax, 3
	speed10:
		sub			eax, 1
	speed5:
		sub			eax, 1
	speed1:
		
		mov			speed, eax
		
		ret

CalcSpeed ENDP

PrintExitReason PROC


; GameOverReason = 1:  				emptyBankS	BYTE "Ran out of bank!", 0
; GameOverReason = 2:				hitWallS		BYTE "Hit walls!", 0
; GameOverReason = 3:				hitEscapeS	BYTE "Hit Escape", 0

	pushad

	movzx			eax, gameOverReason
	cmp				eax, 1
	je				emptyBank
	cmp				eax, 2
	je 				wallHit
	
	mov				edx, 0
	mov				dl, 30
	mov				dh, 12
	call			GotoXY
	mov				edx, OFFSET hitEscapeS
	call			WriteString
	
	popad
	ret
	
	emptyBank:
		mov				edx, 0
		mov				dl, 30
		mov				dh, 12
		call			GotoXY
		mov				edx, OFFSET emptyBankS
		call			WriteString
		
		popad
		ret
		
	wallHit:
		mov				edx, 0
		mov				dl, 30
		mov				dh, 12
		call			GotoXY
		mov				edx, OFFSET hitWallS
		call			WriteString
		
		popad
		ret
PrintExitReason ENDP


PressEscape PROC

	mov			al, 3
	mov			gameOverReason, al
	
	ret

PressEscape ENDP

DrawUI PROC
		pushad

		mov			dx, 0
		call		Gotoxy
		mov			edx, OFFSET scoreS
		call		WriteString
		movzx		eax,	score
		call		WriteDec
		mov			al, ' '
		call		WriteChar
		call		WriteChar
		call		WriteChar
		
		mov			dx, 	0
		mov			dl,	30
		call		Gotoxy
		mov			edx, OFFSET bankS
		call		WriteString
		movzx		eax, bank
		call		WriteDec
		mov			al, ' '
		call		WriteChar
		call		WriteChar
		call		WriteChar
	
		popad
		ret

DrawUI ENDP

PauseGame PROC

	pushad

	infLoop:

		call		ReadKey
		cmp			dx, ' '
		je			exitLoop

	jmp 		infLoop
	
exitLoop:
	
	popad
	ret

PauseGame ENDP
DrawWalls PROC
		mov			ax, walls
	
		mov			edx,	0
		mov			dl,	ah
		mov			dh,	5
		call		Gotoxy
		
		mov			al,	0ddh
		call		WriteChar			; Draw Right Wall
		
		mov 		ax, walls
		mov			edx, 0
		mov			dl, al
		mov			dh,	5
		call		Gotoxy
		
		mov			al,	0deh
		call		WriteChar			; Draw Left Wall
		
		ret
DrawWalls ENDP

ColourWarning PROC

		pushad

		mov			al, delta
		cmp			al, 0
		jg			moveRight

		; Moving Left
		
		mov			ax, walls
		mov			bl, pos
		sub			bl, al
		jmp			compare
		
	moveRight:
		mov			ax, walls
		mov			bl, pos
		sub			bl, ah
		neg			bl
		
		jmp			compare
		
	compare:
	
		push 		eax
		movsx		eax, bl
		mov			dx, 0
		mov			dh, 13
		call		GotoXY
		call		WriteInt
		pop			eax
	
		cmp			bl, 5
		jg			yellowL
		call		SetRedColour
		popad
		ret
		
	yellowL:
		cmp			bl, 15
		jg			whiteL
		call		SetYellowColour
		popad
		ret
	
	whiteL:
		call		SetWhiteColour
		
		popad
		
		ret
	
ColourWarning ENDP

SetRedColour PROC

	push		eax
	
	mov			eax, red
	call		SetTextColor
	
	pop			eax

	ret

SetRedColour ENDP
SetWhiteColour PROC

	push 		eax
	
	mov			eax, white
	call		SetTextColor
	
	pop			eax
	
	ret

SetWhiteColour ENDP
SetYellowColour PROC

	push		eax
	
	mov			eax, yellow
	call		SetTextColor
	
	pop			eax
	
	ret

SetYellowColour ENDP

DrawPlayer PROC

		mov			dh, 5
		mov			dl, pos
		call		Gotoxy
		mov			al, ' '					; Clear previous character
		call		WriteChar
		
		mov			dl, pos
		add			dl, delta
		mov			pos, dl
	
		call		ColourWarning
		
		mov			dh, 5
		mov			dl, pos
		call		Gotoxy
		
		mov			al, plr
		call		WriteChar			; Draw the player in the new location
		
		call		SetWhiteColour
		
		call		PrintDebug

	ret
DrawPlayer ENDP

PrintDebug PROC
		pushad
		
		mov			dl, 0
		mov			dh,	10
		call		GotoXY
		mov			edx, OFFSET deltaDebugS
		call		WriteString
		movsx		eax, delta
		call		WriteInt
		
		mov			dl, 0
		mov			dh, 11
		call		GotoXY
		mov			edx, OFFSET posDebugS
		call		WriteString
		movsx		eax, pos
		call		WriteInt
		
		popad
		
		ret
PrintDebug ENDP

END main